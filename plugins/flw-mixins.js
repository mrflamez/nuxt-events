import Vue from 'vue'
import numeral from 'numeral'

Vue.mixin({
  methods: {
    sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms))
    },
    comparePricing(a, b) {
      return parseFloat(a.price) - parseFloat(b.price)
    },
    getTickets(pricingData) {
      const tickets = []
      pricingData.forEach(pricing => {
        if (pricing.qty > 0) {
          tickets.push(pricing)
        }
      })
      return tickets
    },
    formatAmount(amount) {
      if (isNaN(amount)) {
        amount = 0
      }

      const hasDecimal = amount % 1 !== 0
      if (hasDecimal) {
        return `N${numeral(amount).format('0,0.00', Math.floor)}`
      }
      return `N${numeral(amount).format('0,0', Math.floor)}`
    },
    numberOfTickets(pricingData) {
      let numberOfTickets = 0
      pricingData.forEach(pricing => {
        numberOfTickets += pricing.qty
      })
      return numberOfTickets
    },
    tickets(pricingData) {
      const tickets = {}
      pricingData.forEach(pricing => {
        tickets[pricing.id] = pricing.qty
      })
      return tickets
    },
    getSubtotal(pricingData) {
      let total = 0
      pricingData.forEach(pricing => {
        if (pricing.qty > 0) {
          total += pricing.qty * pricing.price
        }
      })
      return total
    },
    getVAT(pricingData) {
      const vat = 0.07
      return this.getSubtotal(pricingData) * vat
    },
    getTotal(pricingData) {
      return this.getVAT(pricingData) + this.getSubtotal(pricingData)
    }
  }
})
