import Vue from 'vue'
import dayjs from 'dayjs'
import numeral from 'numeral'

Vue.filter('date', date => {
  return dayjs(date).format('DD MMMM YYYY')
})

Vue.filter('datetime', date => {
  return dayjs(date).format('dddd, MMMM DD YYYY, h:mm a')
})

Vue.filter('formatAmount', amount => {
  if (isNaN(amount)) {
    amount = 0
  }

  const hasDecimal = amount % 1 !== 0
  if (hasDecimal) {
    return `N${numeral(amount).format('0,0.00', Math.floor)}`
  }
  return `N${numeral(amount).format('0,0', Math.floor)}`
})
