export default function ({$axios, app, route}) {
  $axios.onRequest(config => {
    config.headers.common['Content-Type'] = 'application/json'
    config.headers.common.Accept = 'application/json'
  })
}
