import {setupCache} from 'axios-cache-adapter'
require('dotenv').config()

const cache = setupCache({
  maxAge: 15 * 60 * 1000
})

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: 'The best events happening now. || Flutterwave Events',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {name: 'theme-color', content: '#F5A623'},
      {
        hid: 'description',
        name: 'description',
        content: 'The best events happening now.'
      }
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/icon.png'},
      {rel: 'stylesheet', href: '/fonts/flw-fonts.css'}
    ],
    script: [{src: 'https://checkout.flutterwave.com/v3.js', body: true}]
  },
  pageTransition: {
    name: 'fade-slide-up',
    mode: 'out-in'
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#F5A623'
  },
  /*
   ** Global CSS
   */
  css: ['@/assets/scss/main.scss'],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: ['@/plugins/axios.js', '@/plugins/flw-mixins.js', '@/plugins/formatters-filters.js', '@/plugins/vuelidate.js'],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: 'https://eventsflw.herokuapp.com/v1/',
    adapter: cache.adapter
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.vue$/,
        loader: 'vue-svg-inline-loader',
        options: {
          /* ... */
        }
      })
    }
  }
}
