export const actions = {
  async nuxtServerInit({commit}, {app, store}) {
    try {
      const {data} = await app.$axios.get('events?page=1&limit=12')

      const events = await Promise.all(
        data.data.events.map(async event => {
          const {data} = await app.$axios.get(`ticket-types/events/${event.id}`)
          event.pricing = data.data.sort(comparePricing)
          return event
        })
      )

      commit('events/ADD_EVENTS', {pageInfo: data.data.pageInfo, events})
    } catch (error) {}
  }
}

const comparePricing = function (a, b) {
  return parseFloat(a.price) - parseFloat(b.price)
}
