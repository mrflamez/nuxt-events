export const state = () => ({
  data: null
})

export const mutations = {
  ADD_EVENTS(state, eventsData) {
    if (state.data == null) {
      return (state.data = eventsData)
    }
    const events = [...state.data.events, ...eventsData.events]
    eventsData.events = events
    state.data = eventsData
  }
}
